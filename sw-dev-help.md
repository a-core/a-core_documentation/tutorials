This file contains some helpful functions for debugging your C/assembly program

### Compiled assembly program
You can view the compiled assembly program with the following command:
`riscv64-unknown-elf-objdump -d sim.elf`
### Symbol table
You can view the symbol table (the addresses where your C variables are stored) with:
`riscv64-unknown-elf-objdump -s sim.elf`
### Memory contents
You can view the static data stored to memory with:
`riscv64-unknown-elf-objdump -t sim.elf`
