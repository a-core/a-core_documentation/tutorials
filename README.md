# tutorials

Getting started guides for A-Core developers.

List of good third-party tutorials and resources:
- Chisel Bootcamp: https://github.com/freechipsproject/chisel-bootcamp
- Chisel book: https://github.com/schoeberl/chisel-book
- Computer Organization and Design RISC-V Edition: The Hardware Software Interface, by Patterson and Hennessy
- Computer Architecture: A Quantitative Approach, by Hennessy and Patterson
