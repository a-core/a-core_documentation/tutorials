# Getting started with A-Core development

This guide attempts to explain the basic development workflow related to A-Core. While this guide aims to be as thorough as possible, it is likely that there are some errors. If you encounter problems, feel free to open an issue and tag a developer, such as @Roenski or @OttoSimola.

# 1. Setup
These steps will familiarize you with the command line interface and by the end of this section you should have a copy of the A-Core source files in your working machine.

## 1.1 Familiarize yourself with the terminal
If you are not familiar with UNIX-style command line interfaces you should learn the basics before proceeding to the following sections. You should at least be able to carry out basic tasks such as navigating the file system, listing files in the current directory, and running shell scripts. To get you started, you can use e.g. [this tutorial](https://web.archive.org/web/20210913234830/https://scicomp.aalto.fi/training/linux-shell-tutorial/) as a starting point.


## 1.2 Prerequisites

Here, you need to select whether you choose to use an ECD cluster computer or your local computer for development.

## 1.2.1 ECD cluster computers
Easiest way to develop is to use ECD department's cluster computers. This ensures that you have all the tools already installed. Use x2go or your favourite ssh client to connect to one of the computers. Many developers use VS Code's Remote Extension. When using ssh, make sure to use X11 forwarding to enable using application GUIs (Graphical User Interfaces). This can be done by using `-Y` flag when running `ssh`. For VS Code's Remote Extension, to enable X11 forwarding you need to, at least, edit your ssh config file. Use internet resources to find out how to do it.

If you choose to use a department computer, you may skip to section 1.3.

## 1.2.2 Local machine
You can also use your local machine for development. However, this introduces some prerequisites for development.

For Chisel, you need Java Development Kit, sbt, and Verilator. See instructions for installing them [here](https://github.com/chipsalliance/chisel3/blob/master/SETUP.md). This setup is enough for compiling Chisel and running Chiseltests, but it is *not* enough for running assembly- or C-programs on the processor.

For running assembly- or C-programs, you need to have installed the RISC-V GNU toolchain and Icarus Verilog. Unfortunately, the GNU toolchain needs to be built from source because our processor hosts a non-standard set of extensions. Steps for installing go as follows:
1. Clone the repository from https://github.com/riscv-collab/riscv-gnu-toolchain.
2. Checkout a release version, e.g. `git checkout 2024.04.12`
3. Open the README in that repository
4. Ensure you have the prerequisites
5. Run 
```
./configure --prefix=/opt/riscv --with-multilib-generator="rv32imf_zicsr-ilp32f--c;rv32im_zicsr-ilp32--c;rv32i_zicsr-ilp32--c;rv32if_zicsr-ilp32f--c;"
```
**NOTE**: change the prefix to a path where you have write access to, e.g. `$HOME/opt/riscv` !

6. Run `make`
7. Add the binaries to PATH after finished.

**Note that the installation can take a very long time (>1 hour).**

You also need Icarus Verilog, since it is the only open-source simulator supported by TheSyDeKick. **The version should be minimum 11.0.** Follow the official installation guides: http://iverilog.icarus.com/


## 1.3 Initial git configuration

You need git accounts and ssh keys (see next section) in the following services:
- gitlab.com
- github.com
- If you work on closed-source stuff:
  - bubba.ecdl.hut.fi (if you work on closed-source stuff)
  - version.aalto.fi (if you work on closed-source stuff)

Before you can create git commits, you need to tell git your name and email address so that it can associate each commit with this information. This can be achieved by typing the following commands into the terminal. Any text inside angle brackets `<like-this-text-here>` is meant to be replaced by appropriate information. The dollar sign symbol `$` is the shell prompt and should be left out when typing these commands to the terminal.

```shell
$ git config --global user.name '<your-name>'
$ git config --global user.email '<your-email>'
```

For example, if your name were Teemu Teekkari you could type the following commands:
```shell
$ git config --global user.name 'Teemu Teekkari'
$ git config --global user.email 'teemu.teekkari@aalto.fi'
```

## 1.4 ssh setup
You need to set up an SSH key to GitLab. See the instructions [here](https://docs.gitlab.com/ee/user/ssh.html). Do *NOT* use an empty passphrase! Select RSA as the authentication algorithm.

Once you have your SSH key, you can set up an ssh agent, so that you don't need to type in your passphrase every time you interact with git by pulling or pushing. Run the following commands:
1. ```eval `ssh-agent` ```
2. `ssh-add path/to/your/ssh_key`

If you saved your ssh key to the standard location, step 2 would be `ssh-add ~/.ssh/id_rsa` if you selected RSA for authentication.

It is recommended that you create an easy-to-remember alias for these two commands. For example, if working on the ECD cluster machine, an alias can be created by adding the following lines to `~/.cshrc` (create the file if it doesn't exist):
```
alias assh 'eval `ssh-agent` && ssh-add ~/.ssh/id_rsa'
```
Now, you can create the ssh-agent by typing `assh` on the command line after re-opening the terminal.

### 1.4.1 ECD cluster computer

You need to add the following lines to your `~/.ssh/config` file:

```
Host gitlab.com
    Hostname altssh.gitlab.com
    Port 443
```

## 1.5. Clone relevant git repositories.

To initialize A-Core development environment, follow these steps:
1. Clone A-Core master project: https://gitlab.com/a-core/a-core_thesydekick/a-core_thesydekick
2. Run the commands pointed in Configuration quickstart (in README.md)
The initialization steps can take a few minutes to complete. Note that if you have not set up the ssh-agent, you will be prompted for your ssh key passphrase multiple times during the process. If this step is successful, you have a fully initialized A-Core development environment!

# 2. Project structure

In the last step of the previous section, you cloned the master project, A-Core TheSyDeKick project. This section only provides a brief overview about the project structure. If you are an impatient person and just want to run some test program on A-Core,  you can skip to section 3.

## 2.1 TheSyDeKick

TheSyDeKick is a Python-based ASIC development and verification framework developed at Aalto University ECD lab. A-Core uses it for automating JTAG programming sequence and RTL simulation flow. More info can be read in its GitHub repository: https://github.com/TheSystemDevelopmentKit

## 2.2 A-Core GitLab structure

A-Core repository is divided into four subgroups:
- A-Core_Chisel - hosts Chisel source code 
- A-Core_Documentation - hosts various documentation, including this tutorial
- A-Core_Software - hosts Assembly- and C-programs, libraries, and initialization scripts for A-Core
- A-Core_TheSyDeKick - hosts A-Core TheSyDeKick project entities

Next, we explain some of these groups in detail.

## A-Core_Chisel

Chisel code is hosted in this group. Needless to say, when you develop any open-source Chisel code for A-Core project, the repository should be here.

Important repositories include:
- ACoreChip: Top-level for A-Core System-on-Chip (SOC). This module connects modules, such as ACoreBase, Program Memory, RAM, and JTAG, to compose a functioning processor.
- ACoreBase: A-Core processor core. This module hosts the CPU itself.
- AMBA: Library for AXI4 memory-mapped modules, including UART, GPIO, DMA, and RAM.
- a_core_common: Common types, interfaces, and configurations used in A-Core designs.

## A-Core_TheSyDeKick

The verification environment for A-Core. 

Important repositories include:
- a-core_thesydekick: Master project. It includes everything that is needed for running and developing A-Core. It instantiates these submodules under `Entities/` folder.
- ACoreChip: A-Core TheSyDeKick Entity. It defines its verification Entity. Also, the source code (Chisel) is located under `chisel/` folder.
- ACoreTestbenches: Testbenches for running A-Core programs on various platforms, including RTL simulation, FPGA, and ASIC.
- ACoreTests: Hosts the GNU make interface for running programs.
- JTAG: Hosts methods for generating JTAG signals for programming the processor.

## A-Core_Software

Software for A-Core. For compiling software programs, you need to have the RISCV GNU toolchain installed, as described earlier.

Important repositories include:
- a-core-library: Hosts C-code for common A-Core functionalities, such as printing, setting up UART, setting up GPIOs, startup scripts, linker scripts, default Makefiles, etc.
- riscv-assembly-tests: Hosts programs written in RISC-V assembly. 
- riscv-c-tests: Hosts programs written in C.
- riscv-assembly-template: Hosts an example assembly program. You can start developing a new assembly program by copying the contents of this repository.
- riscv-c-template: Hosts an example C program. You can start developing a new C program by copying the contents of this repository.

# 3. Running a program on A-Core

This section provides a hands-on tutorial on how to run a software (Assembly or C) program on A-Core.

1. Ensure you have a clean, initialized repository (section 1.4)
2. Run `source sourceme.csh`, if using ECD lab computer. If using local computer, make sure all the required tools have been added to PATH (GNU toolchain, Verilator, Icarus Verilog, pip)
3. Run `./pip3userinstall.sh` if not yet run. This script installs required Python packages and compiles C-accelerated Python modules (such as jtag-kernel).
4. Navigate to `Entities/ACoreTests`.
5. Run `./configure`

Now, everything has been set up for running a program. You will run tests with `make` and providing arguments for the test. You can run `make help` for information about the different arguments. Run, for example, the following:
```
make test_config=m-tests simulator=thesdk sim_backend=icarus
```
This will run the assembly program located under `Entities/ACoreTests/sw/riscv-assembly-tests/src/m-tests`. You should see many status messages from TheSyDeKick in the terminal. Also, you should see the test program printing status messages, such as `OK` or `ERR`. The simulation does not open or generate a waveform by default: if you want to see the waveform, run the same command with option `interactive_sim=true`.

Further info about the tests, and how to add new tests, can be found in [ACoreTests README](https://gitlab.com/a-core/a-core_thesydekick/ACoreTests).

# 4. Conclusion
This marks the end of the Getting Started guide. Now, you have, hopefully, succesfully executed a software program on A-Core. It is time to move to other tutorials.
