# A-Core Chisel guide

## 1. Introduction
This guide explains how to develop Chisel with A-Core. It aims to answer the following questions:
1. How to make changes to source code?
2. How to test new changes to source code?
3. How git version control works in this project?
4. How Scala libraries work in this project?

This guide assumes you have read the Getting Started tutorial.

## 2. Making changes to source code
Once you have cloned the `a-core_thesydekick` repository and executed the necessary initializations, you can navigate to `Entities/ACoreChip/chisel`. This is where the ACoreChip Chisel project resides. Scala source code is **always** located under `src/main/scala`. This is a standard location and one should not deviate from it. You can find `acorechip.scala` from that location. This is the top level for ACoreChip project.

ACoreChip project contains also subprojects that have been added to the git repository as git submodules. These include ACoreBase, JTAG, and ProgMem. These are also standalone projects, all of which have their own `src/main/scala` folders. You can find the source code for these modules there.

To modify source code, simply modify these `.scala` files.

## 3. Testing source code changes
### 3.1 Compile only
Once you have modified some of the projects, you need to re-compile the project. The project can be compiled with `make`. All projects have their own Makefile that needs to be generated with `./configure`. So, to re-compile the ACoreChip project, including the subprojects, you can run `./configure && make clean && make ACoreChip`. This command will generate Verilog under `/verilog` folder.

The `make` command has numerous parameters, such as the set of extensions for the processor, that you can set. Observe the generated Makefile to find out all possibilities. The default generates the processor with only the base I-extension. 

You can also compile chisel from ACoreTests. Run `make help` there to find further help. 

### 3.2 Chiseltest
Some Chisel modules, e.g. ACoreBase, have Chiseltests for testing the modules. If they do, you can run them using `sbt test` command. You can run specific test classes with `sbt 'testOnly <test-class>`. For example, to run MultiplierBlockSpec tests, run `sbt 'testOnly acorebase.MultiplierBlockSpec'`.  

### 3.3 C- or assembly-tests
When you have made changes to source code, you can simply re-run the flow that runs C and Assembly programs (the one described in Getting Started tutorial). The flow will automatically re-compile the project. However, if you made changes to the chisel source code, you need to clean and compile first before re-running the flow.

### 3.4 Viewing waveforms
To debug your design, it is useful to look at the waveforms your simulation produces. If you run C or assembly programs in TheSyDeKick environment, you can run `make wave` after simulation finishes, assuming you selected the correct flags when running the simulation (see `make help`).

For chiseltests, you need to add `.withAnnotations(Seq(WriteVcdAnnotation))` to your `test()` function. For example:
```
test(new PCBlock(XLEN=32, pc_init="h0001_0000")).withAnnotations(Seq(WriteVcdAnnotation))
```
A vcd file is generated under `test_run_dir` folder after running the test. You can open it with gtkwave
```
gtkwave path/to/waveform.vcd
```

## 4. Updating Git after changes
This project uses git submodules for robust version control. This robustness brings some extra steps for when you are ready to commit your changes to git. This is best described by an example. Let's consider that we are modifying ACoreBase. The project structure looks as follows:
```
- a-core_thesydekick
  |- ACoreChip (submodule, points to a commit in ACoreChip TheSydeKick repository)
    |- chisel (submodule, points to a commit in ACoreChip Chisel repository)
      |- ACoreBase (submodule, points to a commit in ACoreBase Chisel repository)
```
When you make changes to ACoreBase and push the commit, you need to update the chisel repository to point to the new commit. This is done by `git add ACoreBase && git commit -m "Update ACoreBase" && git push`. However, this is not enough, as ACoreChip repository still points to the old commit in chisel repository. Therefore, you need to similarly update chisel in ACoreChip repository. As a final step, you need to update ACoreChip in a-core_thesydekick repository.

However, when you start to develop a new feature, you should not push directly to `master`, but instead create a development branch. Let's say that we start developing a pipeline optimization, so we could create a development branch called `pipeline-opt` to ACoreBase. *You should create the same development branch also to ACoreChip(Chisel), ACoreChip(TheSyDeKick), and a-core_thesydekick!*. In this way, if you encounter a problem you cannot solve, the person you asked to help you can simply clone `a-core_thesydekick` repository, check out your development branch, and run `./init_submodules.sh` to get the same workspace as you are having. 
- Keep in mind that you need to update the submodules in your development branches as explained in the previous paragraph. It's not necessary after every commit, but at least when you ask for help you should do it!
- You can check whether your submodules are updated by running `git status`. If a submodule is red in color and has a note `(new commits)`, this means that the module is not in sync and should be updated at some point
  -  Another possible note is `(modified content)`. This means that the submodule has uncommitted changes.
- After initializing the a-core_thesydekick, all submodules are in a `detached HEAD` state. Before making commits, it is best to checkout some branch, e.g. `git checkout master`.

## 5. Scala libraries

There are two projects that are not handled as submodules, but as Scala libraries: amba and a_core_common. Both projects are imported multiple times in the ACoreChip project. The libraries are hosted in A-Core Gitlab registry.

### 5.1 Using the libraries in a project
One can use the libraries by adding the following line to project's `build.sbt`:
```
// 13348068 is the group ID for A-Core group in Gitlab
resolvers += "A-Core Gitlab" at "https://gitlab.com/api/v4/groups/13348068/-/packages/maven"
```

Then, you can add them as libraryDependencies:
```
libraryDependencies ++= Seq(
    "Chisel-blocks"   %% "amba" % 0.1+,
    "Chisel-blocks"   %% "a_core_common" % 0.1+
)
```
Then, you can use `import amba` and `import a_core_common` in your Chisel project. See [Section 5.3](https://gitlab.com/a-core/a-core_documentation/tutorials/-/blob/master/a-core_chisel_guide.md?ref_type=heads#53-versioning) for information about version numbers.

### 5.2 Developing the library
If you need to develop one of the libraries, you need to understand how publishing works. One can publish Scala libraries in two places: locally and to a remote repository. Local publishing is done with `sbt publishLocal`, whereas remote publishing occurs with another command that is not mentioned here because **you should not publish packages to remote repository manually!**. We have a CI job in all libraries that automatically publish newest versions to Gitlab registry.

When you wish to develop a library, the workflow goes as follows:
1. Clone `a-core_thesydekick` and the developed library repository
2. Create a new branch in the library repository
3. Make modifications in the library repository
4. Commit (and push) your changes
5. Run `sbt publishLocal` in the library repository
    - this step publishes a new version of your library to local maven repository
6. Run `sbt clean` in `a-core_thesydekick` or `ACoreChip` (Chisel)
    - this step cleans previous `a_core_common` from cache
7. Run your chisel generator (`make chisel/<hw-config>` in ACoreTests, `make ACoreChip` in ACoreChip (Chisel) etc.)

When a project is compiled, Scala libraries are searched from the following places in this order:
1. Local repository. If you have run `sbt publishLocal`, you will have a locally published version. This is located in `~/.ivy2` folder, so if you seek to delete a locally published version, you can do it by deleting that folder.
2. Remote repository, e.g. Gitlab registry for A-Core libraries.

This means that if you have locally published versions, they will be used even if there are newer versions in the remote repository. Therefore, you can run `sbt publishLocal` for the library you are developing, and then that version should be visible for the projects that are using the library.

- Sometimes, you might encounter problems with pulling the version you desire. This can be caused by cached versions. If you need a sanity check, you can delete all cached versions by deleting `~/.ivy2` (locally published versions) and/or `~/.cache/coursier` (versions pulled from remote repositories) folders.

The library will be pushed to Gitlab registry automatically after it gets merged to the master branch.

How to make Metals fetch the newest, locally published version:
1. Disable Metals extension and reload window
2. Delete `.bloop`, `.metals`, and `project/.bloop` folders
3. Delete all previous libraries from under `~/.ivy2`
4. Locally publish the new library version
5. Enable Metals and run Import Build

### 5.3 Versioning
Versioning for the published libraries happens automatically. A new version is published for each new commit in master. The version is generated from `git describe` command that checks the tags present in the repository. If the commit is the same as what the highest tag points to, then version number is the same as the tag. Every new commit will get a suffix that indicates how many commits have passed since the tag, and the commit hash. E.g., if the highest tag in the repository is `v0.1`, a commit with a hash `cf45abcd` that is two commits away from the tag would have a version `v0.1-2-cf45abcd`.

The projects that use libraries have been configured to pull the version `v0.1+`. This means that, assuming the versioning explained in the previous paragraph, the modules will automatically pull the latest commit published under the tag `v0.1` in master. When needed, the projects can also be set to follow an exact version number. If a new tag is published, e.g. `v0.2`, the used version is the last commit before the new tag.

### 5.4 Increasing library version number
**The library version number should be increased if you introduce changes that are not backwards-compatible.** For example, if building any A-Core version, that uses that version number, fails, then the version number must be increased.

The flow for increasing the version number goes as follows:
1. Merge your changes to master
2. A new pipeline will be automatically triggered. **Abort the pipeline!** On the left panel select `Build -> Pipelines -> Select the pipeline -> Stop the pipeline`
    - If you are too late and the "publish" job manages to finish, go to `Deploy -> Package Registry` and delete the latest package
3. Publish a new tag with the new version number. You can do it via command line or in the GUI (`Code -> Tags`). Add a description about what new there is that is backwards-incompatible.
4. Start a new pipeline for the master branch. `Build -> Pipelines -> Run pipeline`

## 6. Conclusion
Hopefully, after reading and comprehending this tutorial, you are able to develop your features with few problems. However, if you encounter troubles or don't understand something, feel free to ask someone who has been using the flow longer.


